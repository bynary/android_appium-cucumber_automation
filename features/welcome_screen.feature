@welcome_screen
Feature: Tests for Welcome Screen functionality


  Scenario: First time start the app - welcome screen should be shown
    Then I should see "WELCOME TO" text
    And I should see "Shop your favorite store on the go!" text

  Scenario: Let's Go and Privacy Practices buttons should be shown
    Then I should see Lets Go button
    And I should see Privacy Practices button

  Scenario: Let's Go button should lead to Shop Smarter screen
    Then I should see Lets Go button
    When I press on Let's Go button
    Then I should see Shop Smarter screen

  @ready-to-go
  Scenario: Privacy Practices button should lead to the home screen
    Then I should see Privacy Practices button
    When I press on Privacy Practices button
    Then I should see Privacy Practices screen


  Scenario: Landing to Home Screen through
            Let's Go button > Shop Smarter screen > Not Now button > Stay Connected screen
    Then I should see Lets Go button
    When I press on Let's Go button
    Then I should see Shop Smarter screen
    When I press Not Now button
    Then I should see Stay Connected screen
    When I press Not Now button
    Then I should see Home Screen

  Scenario: Landing to Home Screen through
            Let's Go button > Shop Smarter screen > Not Now button > Stay Connected screen
    Then I should see "WELCOME TO" text
    And I should see "Shop your favorite store on the go!" text
    And I should see Lets Go button
    When I press on Let's Go button
    Then I should see Shop Smarter screen
    When I press Not Now button
    Then I should see Stay Connected screen
    When I press Not Now button
    Then I should see Home Screen

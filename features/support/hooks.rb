Before do
  puts "Before hook is executed"
  puts "Start driver"
  $driver.start_driver
end

After do
  puts "After hook is executed"
  puts "driver quit"
  sleep 3
  $driver.driver_quit
end
require "appium_lib"

def caps
  { caps: {
      deviceName: "Moto X4",
      platformName: "Android",
      app: (File.join(File.dirname(__FILE__), "com.macys.android.apk")),
      appPackage: "com.macys.android",
      appActivity: "com.macys.main.ui.activity.MainActivity",
      newCommandTimeout: "3600"
  }
  }
end

# noinspection RubyArgCount
Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object
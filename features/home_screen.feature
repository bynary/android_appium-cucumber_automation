@home_screen
Feature: Tests for Home screen functionality

  @wip
  Scenario: Left Nav Menu Should be accessible from Home Screen
    Then I should see "WELCOME TO" text
    And I should see "Shop your favorite store on the go!" text
    And I should see Lets Go button
    When I press on Let's Go button
    Then I should see Shop Smarter screen
    When I press Not Now button
    Then I should see Stay Connected screen
    When I press Not Now button
    Then I should see Home Screen

    When I press Burger button
    Then I should see Left Nav menu

    When I press "Offers" button
    Then I should see "My Saved Offers" text
    And I should see "Deals & Promotions" text
    And I should see "Add Offer" text

    When I press "Account" button
    Then I should see "My Account" text
    And I should see "Inbox" text
    And I should see "Order History" text
    And I should see "Wallet" text
    #And I should see "Star Rewards" text
    And I should see "Plenti" text
    And I should see "Address Book" text
    And I should see "Preferences" text

    When I press "Registry" button
    Then I should see "My Registry" text

    When I press "Stores" button
    Then I should see "Find a Store" text

    When I press "More" button
    Then I should see "App Settings" text

    When I press "App Settings" button
    #Then I should see "Notifications" text

    When I press Push Notifications checkbox
    Then I verify Push Notification checkbox is checked

    When I press Push Notifications checkbox
    Then I verify Push Notification checkbox is unchecked

    When I press Burger button
    Then I press "App Feedback" button
    Then I should see "Feedback" text
    Then I verify button Submit button is disabled
    Then I enter MyText to Feedback Editbox
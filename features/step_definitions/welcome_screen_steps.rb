Then(/^I should see "([^"]*)" text$/) do |text|
  text(text)
end

Then(/^I should see Privacy Practices button$/) do
  find_element(id: "btnPrivacyPolicy")
end

And(/^I should see Lets Go button$/) do
  find_element(id: "btnSecond")
end

When(/^I press on Let's Go button$/) do
  find_element(id: "btnSecond").click
end

When(/^I press on Privacy Practices button$/) do
  find_element(id: "btnPrivacyPolicy").click
end

Then(/^I should see Privacy Practices screen$/) do
  text("Privacy Practices")
end

Then(/^I should see Shop Smarter screen$/) do
  text("SHOP SMARTER")
end

When(/^I press Not Now button$/) do
  find_element(id: "btnSecond").click
end

Then(/^I should see Stay Connected screen$/) do
  text("STAY CONNECTED")
end

Then(/^I should see Home Screen$/) do
  find_element(id: "search_src_text")
end
When(/^I press Burger button$/) do
  find_element(id: "navigate_up").click
end

Then(/^I should see Left Nav menu$/) do
  find_element(accessibility_id: "Shop button")
end

When(/^I press Offers button$/) do
  find_element(accessibility_id: "Offers button").click
end

When(/^I press "([^"]*)" button$/) do |button_name|
  content_desc = button_name + " button"
  find_element(accessibility_id: content_desc).click
end

When(/^I press Push Notifications checkbox$/) do
  find_element(id: "sw_push").click
end

Then(/^I verify Push Notification checkbox is (checked|unchecked)$/) do |state|
  if state == "checked"
    find_element(id: "sw_push").attribute('checked') == true
  elsif state == "unchecked"
    find_element(id: "sw_push").attribute('checked') == false
  end
end

Then(/^I verify button Submit button is (enabled|disabled)$/) do |state|
  attr = find_element(id: "action_submit").attribute('enabled')
  if state == 'enabled'
    attr == true
  elseif state == 'disabled'
    attr == false
  end
end

Then(/^I enter MyText to Feedback Editbox$/) do
  find_element(id: "edt_user_feedback").send_keys("Hi Macy's! My name is Yuriy Bolotnyy. ")
  find_element(id: "edt_user_feedback").send_keys("I am ready to create Appium framework for you!")
end